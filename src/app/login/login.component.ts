import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import {Router} from '@angular/router';
import {TokenService} from '../token.service';
import {HttpErrorResponse} from '@angular/common/http';
import {WsseService} from '../wsse.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  $login: Observable<{ secret?: string }>;
  username: string;
  secret: string;
  created: string;
  error: string;

  credentialsForm = new FormGroup({
    username: new FormControl(''),
    password: new FormControl('')
  });

  constructor(public wsseService: WsseService, private tokenService: TokenService, private router: Router) { }

  ngOnInit() {
    if (this.tokenService.hasAuthorizationToken()) {
      this.username = this.tokenService.username;
      this.created = this.tokenService.created;
      this.secret = this.tokenService.secret;
    }
  }

  onSubmit() {
    const credentials: { username: string; password: string } = this
      .credentialsForm.value;

    // Login should return user secret (hashed password)
    this.$login = this.wsseService.postCredentials(credentials);

    this.$login.subscribe(
      // Show generated token
      ({ secret }) => {
        this.username = credentials.username;
        this.created = this.tokenService.formatDate(new Date());
        this.secret = secret;

        const authToken = this.tokenService.generateWSSEToken(
          this.username,
          this.created,
          this.secret
        );

        console.log(`Generated WSSE Token ${authToken}`);
      },
      // Show server error
      (error: HttpErrorResponse) => {
        console.error(error);
        this.error = error.message;
      }
    );
  }

  onLogout() {
    this.tokenService.cleanAuthorizationToken();
    this.secret = null;
    return this.router.navigate(['']);
  }

}
