import { TestBed } from '@angular/core/testing';

import { WsseService } from './wsse.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('WsseService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]
  }));

  it('should be created', () => {
    const service: WsseService = TestBed.get(WsseService);
    expect(service).toBeTruthy();
  });
});
