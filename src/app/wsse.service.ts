import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../environments/environment';

interface credentialsType {
  username: string;
  password: string;
}

@Injectable({
  providedIn: 'root'
})
export class WsseService {

  constructor(private httpClient: HttpClient) { }

   postCredentials(credentials: credentialsType): Observable<{ secret?: string }> {
    return this.httpClient.post(environment.server + '/login', credentials );
  }
   get_hello(): Observable<{hello?: string}> {
    return this.httpClient.get(environment.server + 'api/hello');
  }
}
